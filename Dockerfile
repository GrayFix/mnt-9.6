FROM python:3.8-alpine

WORKDIR /python_api

COPY requirements.txt /tmp
RUN pip install --no-cache-dir -r /tmp/requirements.txt

COPY python-api.py /python_api

EXPOSE 5290
CMD ["python", "python-api.py"]
