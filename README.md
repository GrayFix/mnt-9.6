# 09.06 Gitlab
1. Часть `DevOps` - создан `Dockerfile` и файл `.gitlab-ci.yml` с описанием создания Docker контейнера и его сборке
2. Часть `Product Owner` - создан нужный [Issue](https://gitlab.com/GrayFix/mnt-9.6/-/issues/1)
3. Часть `Developer` - внесены изменения и создан [Merge Request](https://gitlab.com/GrayFix/mnt-9.6/-/merge_requests/2)
4. Часть `Tester` - поднят docker контейнер, протестировано, [отписано в Merge Request](https://gitlab.com/GrayFix/mnt-9.6/-/issues/1#note_804927710) и Issue закрыт
5. Необязательная часть - `.gitlab-ci.yml` добавлена часть с тестированием получившегося образа. Настроена проверка `curl` метода `/get_info` на возврат 200 кода
